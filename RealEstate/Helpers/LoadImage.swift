//
//  LoadImage.swift
//  RealEstate
//
//  Created by CodeGradients on 16/07/2020.
//  Copyright © 2020 Code Gradients. All rights reserved.
//

import Foundation
import Kingfisher

class LoadImage {
    
    func load(imageView: UIImageView, url: String) {
        if url.isEmpty {
            return
        }
        
        let imageView = imageView
        let url = URL(string: url)
        let processor = DownsamplingImageProcessor(size: imageView.bounds.size)
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(
            with: url,
            placeholder: UIImage.init(named: "placeholder"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
    }
}
