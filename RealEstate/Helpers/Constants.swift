//
//  Constants.swift
//  RealEstate
//
//  Created by Muhammad Umair on 18/05/2020.
//  Copyright © 2020 Code Gradients. All rights reserved.
//

import Foundation
import UIKit
import EasyTipView

class Constants {
    
    static var mineId = ""
    static var admin_email = ""
    
    static let app_link = "https://www.rentalpropertydashboard.com"
    
    static var currencies = ["US Dollar\nUSD - $", "Australian Dollar\nAUD - A$", "European Euro\nEUR - €", "Japanese Yen\nJPY - ¥", "British Pound\nGBP - £"]
    static var currency_placeholder = "$"
    static var currency_code = "USD"
    
    static var label_tip_views = [EasyTipView]()
    
    static func getCurrentMillis() -> String {
        return String(Int64(Date().timeIntervalSince1970 * 1000))
    }
    
    static func getMillis(_ date: Date?) -> String {
        if let d = date {
            return String(Int64(d.timeIntervalSince1970 * 1000))
        }
        
        return ""
    }
    
    static func getMillis() -> Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    static func buildDatefromMillis(millis: String) -> Date? {
        
        if !millis.isEmpty {
            if let interval = TimeInterval(millis) {
                return Date(timeIntervalSince1970: interval / 1000)
            }
        }
        
        return nil
    }
    
    static func formatDate(_ fm: String, dt: Date) -> String {
        let df = DateFormatter()
        df.dateFormat = fm
        return df.string(from: dt)
    }
    
    static func formatStringDate(_ fm: String, dt: String) -> Date {
        let df = DateFormatter()
        df.dateFormat = fm
        return df.date(from: dt)!
    }
    
    static func formatNumber(number: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currency
        
        formatter.currencyCode = currency_code
        formatter.maximumFractionDigits = 2
        return formatter.string(from: NSNumber(value: number))!
    }
    
    static func getProfitBackgroundColor(val: Double) -> UIColor {
        let vv = String(format: "%.01f", val)
        let v = Double(vv) ?? 0.0
        if v > 0 {
            return #colorLiteral(red: 0.03137254902, green: 0.6941176471, blue: 0.4117647059, alpha: 1)
        } else if v < 0 {
            return #colorLiteral(red: 1, green: 0.4470588235, blue: 0.4352941176, alpha: 1)
        } else if v == 0 {
            return #colorLiteral(red: 0.9098039216, green: 0.9098039216, blue: 0.9098039216, alpha: 1)
        }
        
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    static func getProfitForegroundColor(val: Double) -> UIColor {
        let vv = String(format: "%.01f", val)
        let v = Double(vv) ?? 0.0
        if v == 0 {
            return .darkText
        } else {
            return #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
    
    static func deviceWidth() -> CGFloat {
        return UIScreen.main.bounds.width
    }
    
    static func getBedRoomsDataList() -> [String] {
        var d = [String]()
        
        for i in 1...10 {
            d.append(String(i))
        }
        
        return d
    }
    
    static func getBathRoomsDataList() -> [String] {
        var d = [String]()
        
        for i in stride(from: 0.5, to: 5.5, by: 0.5) {
            d.append(String(i))
        }
        
        return d
    }
    
    static func getSquareFeetDataList() -> [String] {
        var d = [String]()
        
        for i in stride(from: 100, to: 5100, by: 100) {
            let min = i - 100
            d.append("\(min) - \(i)")
        }
        
        return d
    }
}
