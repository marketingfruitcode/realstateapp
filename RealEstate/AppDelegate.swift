//
//  AppDelegate.swift
//  RealEstate
//
//  Created by Muhammad Umair on 18/05/2020.
//  Copyright © 2020 Code Gradients. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import UserNotifications
import DropDown
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate{
    var appVersion = ""
    var artistViewUrl = ""
    var window: UIWindow?
    enum VersionError: Error {
        case invalidResponse, invalidBundleInfo
    }
    func isUpdateAvailable() throws -> Bool {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
            throw VersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            if let artistViewUrl = result["artistViewUrl"] as? String{
            self.artistViewUrl = artistViewUrl
            }
            if let version = result["version"] as? String{
            self.artistViewUrl = version
            }
            return version != currentVersion
        }
        throw VersionError.invalidResponse
    }

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        let str = "$4,102.33"
//
//        let formatter = NumberFormatter()
//        formatter.numberStyle = .currency
//
//        if let number = formatter.number(from: str) {
//            let amount = number.decimalValue
//            print(amount)
//        }
        
        
        
       
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        
        GMSPlacesClient.provideAPIKey("AIzaSyC-k19HaIvxR2196cQP4-eLmMlGcxvg6vs")
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        UNUserNotificationCenter.current().delegate = self
        //
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        DropDown.startListeningToKeyboard()
        
        
        DispatchQueue.global().async {
            do {
                let update = try self.isUpdateAvailable()
                DispatchQueue.main.async {
                    // show alert
//                   self.showAppUpdateAlert( Force: true)

                }
            } catch {
                print(error)
            }
        }
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let info = response.notification.request.content.userInfo
        let pk = info["pk"] as? String ?? ""
        let uk = info["uk"] as? String ?? ""
        if !pk.isEmpty && !uk.isEmpty {
            Database.database().reference().child("properties").child(pk).observeSingleEvent(of: .value) { (snap) in
                var model = PropertyModel()
                
                model.key = snap.childSnapshot(forPath: "key").value as? String ?? ""
                model.user = snap.childSnapshot(forPath: "user").value as? String ?? ""
                model.address = snap.childSnapshot(forPath: "address").value as? String ?? ""
                model.purchase_date = snap.childSnapshot(forPath: "purchase_date").value as? String ?? Constants.getCurrentMillis()
                model.purchase_amt = snap.childSnapshot(forPath: "purchase_amt").value as? Double ?? 0.0
                model.prop_type = snap.childSnapshot(forPath: "prop_type").value as? String ?? "Single Family"
                model.millis = snap.childSnapshot(forPath: "millis").value as? String ?? ""
                
                var unit = UnitModel()
                
                unit.key = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).key
                unit.unit_name = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "name").value as? String ?? ""
                unit.bedrooms = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "bedrooms").value as? Int ?? 0
                unit.bathrooms = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "bathrooms").value as? Int ?? 0
                unit.square_feet = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "square_feet").value as? Int ?? 0
                unit.rent_month  = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "rent_month").value as? Double ?? 0.0
                unit.rent_annual = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "rent_annual").value as? Double ?? 0.0
                unit.rent_start = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "rent_start").value as? String ?? ""
                unit.rent_end = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "rent_end").value as? String ?? ""
                unit.rent_day = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "rent_day").value as? Int ?? 1
                unit.month_ins = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "month_ins").value as? Double ?? 0.0
                unit.annual_ins = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "annual_ins").value as? Double ?? 0.0
                unit.month_prot = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "month_prot").value as? Double ?? 0.0
                unit.annual_prot = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "annual_prot").value as? Double ?? 0.0
                unit.month_mtg = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "month_mtg").value as? Double ?? 0.0
                unit.annual_mtg = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "annual_mtg").value as? Double ?? 0.0
                unit.month_vac = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "month_vac").value as? Double ?? 0.0
                unit.annual_vac = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "annual_vac").value as? Double ?? 0.0
                unit.month_repair = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "month_repair").value as? Double ?? 0.0
                unit.annual_repair = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "annual_repair").value as? Double ?? 0.0
                unit.month_prom = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "month_prom").value as? Double ?? 0.0
                unit.annual_prom = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "annual_prom").value as? Double ?? 0.0
                unit.month_util = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "month_util").value as? Double ?? 0.0
                unit.annual_util = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "annual_util").value as? Double ?? 0.0
                unit.month_hoa = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "month_hoa").value as? Double ?? 0.0
                unit.annual_hoa = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "annual_hoa").value as? Double ?? 0.0
                unit.month_other = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "month_other").value as? Double ?? 0.0
                unit.annual_other = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "annual_other").value as? Double ?? 0.0
                unit.notes = snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "notes").value as? String ?? ""
                
                var list = [RentRollModel]()
                for sn in snap.childSnapshot(forPath: "units").childSnapshot(forPath: uk).childSnapshot(forPath: "rent_rolls").children {
                    let data = (sn as! DataSnapshot)
                    
                    var md = RentRollModel()
                    md.key = data.key
                    md.amount = data.childSnapshot(forPath: "amount").value as? Double ?? 0.0
                    md.late_fee = data.childSnapshot(forPath: "late_fee").value as? Double ?? 0.0
                    md.year = data.childSnapshot(forPath: "year").value as? Int ?? 2020
                    md.month = data.childSnapshot(forPath: "month").value as? Int ?? 0
                    md.paid = data.childSnapshot(forPath: "paid").value as? Bool ?? false
                    md.total_amount = md.amount + md.late_fee
                    md.image = data.childSnapshot(forPath: "image").value as? String ?? ""
                    
                    list.append(md)
                }
                unit.rent_roll_list = list
                
                if let purchase_date = Constants.buildDatefromMillis(millis: model.purchase_date) {
                    let year = Date().year
                    let month = Date().month - 1

                    var options = DateComponents()
                    options.year = year
                    options.month = month + 1
                    let current = Calendar.current.date(from: options)

                    if current!.timeIntervalSince1970 > purchase_date.timeIntervalSince1970 {
                        let mod = self.getRentRollModel(model: unit, year: year, month: month)

                        let rent = AppStoryboard.Utils.shared.instantiateViewController(withIdentifier: AddRentRollVC.storyboard_id) as? AddRentRollVC
                        rent?.modalPresentationStyle = .custom
                        AddRentRollVC.model = RentRollUnitModel(unit_model: unit, prop_model: model)
                        AddRentRollVC.rent_roll_model = mod
                        
                        if var rootViewController = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.window?.rootViewController {
                            while let top = rootViewController.presentedViewController {
                                rootViewController = top
                            }
                            
                            rootViewController.present(rent!, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
        print("response received")
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
    }
    
    func getRentRollModel(model: UnitModel, year: Int, month: Int) -> RentRollModel {
        for mod in model.rent_roll_list {
            if mod.year == year {
                if mod.month == month {
                    return mod
                }
            }
        }
        
        if let start = Constants.buildDatefromMillis(millis: model.rent_start) {
            if let end = Constants.buildDatefromMillis(millis: model.rent_end) {
                var components = DateComponents()
                components.year = year
                components.month = month + 1
                components.day = Calendar.current.component(.day, from: Date())
                
                if let current = Calendar.current.date(from: components) {
                    
                    
                    var md = RentRollModel()
                    md.key = ""
                    md.amount = model.rent_month
                    md.late_fee = 0.0
                    md.total_amount = model.rent_month
                    md.paid = false
                    md.year = year
                    md.month = month
                    
                    
                    if current.isBetween(start: start, end: end) {
                        return md
                    }
                    
                    if current.isInSameDay(date: start) {
                        return md
                    }
                    
                    if current.isInSameDay(date: end) {
                        return md
                    }
                    
                    if current.isInSameMonth(date: start) {
                        return md
                    }
                    
                    if current.isInSameMonth(date: end) {
                        return md
                    }
                }
            }
        }
        
        var md = RentRollModel()
        md.key = ""
        md.amount = 0.0
        md.late_fee = 0.0
        md.total_amount = 0.0
        md.paid = false
        md.year = year
        md.month = month
        return md
    }
}

extension AppDelegate {
    
    @objc fileprivate func showAppUpdateAlert( Force: Bool) {
        let appName = Bundle.appName()

        let alertTitle = "New Version"
        let alertMessage = "\(appName) Version \(self.appVersion) is available on AppStore."
//        let alertMessage = "New Version is available on AppStore."

        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)

        if !Force {
            let notNowButton = UIAlertAction(title: "Not Now", style: .default)
            alertController.addAction(notNowButton)
        }

        let updateButton = UIAlertAction(title: "Update", style: .default) { (action:UIAlertAction) in
            guard let url = URL(string: self.artistViewUrl) else {
                return
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }

        alertController.addAction(updateButton)
        let blankViewController = UIViewController()
               blankViewController.view.backgroundColor = UIColor.clear

               let window = UIWindow(frame: UIScreen.main.bounds)
               window.rootViewController = blankViewController
               window.backgroundColor = UIColor.clear
               window.windowLevel = UIWindow.Level.alert + 1
               window.makeKeyAndVisible()
               self.window = window
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {

            blankViewController.present(alertController, animated: true, completion: nil)
        }
    }
}
extension Bundle {
    static func appName() -> String {
        guard let dictionary = Bundle.main.infoDictionary else {
            return ""
        }
        if let version : String = dictionary["CFBundleName"] as? String {
            return version
        } else {
            return ""
        }
    }
}
