//
//  RentRollUnitModel.swift
//  RealEstate
//
//  Created by CodeGradients on 12/08/2020.
//  Copyright © 2020 Code Gradients. All rights reserved.
//

import Foundation

struct RentRollUnitModel {
    
    var unit_model: UnitModel
    var prop_model: PropertyModel
}
